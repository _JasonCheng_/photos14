package photos.src.Photos;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.TextArea;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.stage.Stage;

import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;

/**
 * Photo Class
 * Contains fields/methods for a photo (Stock or User Account)
 *
 * @author Ashleigh Chung
 * @author Jason Cheng
 */
public class Photo implements Serializable {
    /**
     * used for serialization
     */
    static final long serialVersionUID  = 1L;

    /**
     * Photo's caption
     */
    String caption;

    /**
     * List of tags for a photo
     */
    ArrayList<Tag> listOfTags;

    /**
     * Date it was "taken"
     */
    Date date;

    /**
     * Path to image
     */
    String path;

    /**
     * photoDisplay field
     */
    @FXML
    ImageView photoDisplay;

    /**
     * photoDetails Field
     */
    @FXML
    TextArea photoDetailsField;

    /**
     * Photo constructor
     */
    public Photo(){
        //Do not delete for FXML
    }

    /**
     * Photo constructor
     *
     * @param caption
     * @param date
     */
    public Photo(String caption, Date date){
        this.caption = caption;
        this.date = date;
        this.listOfTags = new ArrayList<Tag>();
    }

    /**
     * Photo constructor
     *
     * @param date
     * @param Path
     */
    public Photo(Date date, String Path){
        this.date = date;
        this.path = Path;
        this.listOfTags = new ArrayList<Tag>();
    }

    /**
     *
     * @return Photo's path
     */
    public String getPath(){
        return path;
    }

    /**
     * @return caption
     */
    public String getCaption(){
        if(caption == null){
            return "";
        }
        return caption;
    }

    /**
     * @return listOfTags
     */
    public ArrayList<Tag> getListOfTags(){
        return listOfTags;
    }

    /**
     * prints the list of tags
     *
     * @return
     */
    public String printListOfTags(){
        /**
         * To initialize the string
         */
        String tags = "";
        for(int i = 0; i < this.getListOfTags().size(); i++){
            tags += this.getListOfTags().get(i) + "\n";
        }
        return tags;
    }

    /**
     * Returns a tag based on the full string
     *
     * @param t
     * @return tag
     */
    public Tag getTag(String t){
        for(int i = 0; i < listOfTags.size(); i++){
            if(listOfTags.get(i).fullTag.equals(t))
                return listOfTags.get(i);
        }
        return null;
    }

    /**
     * @return when the photo was taken
     */
    public Date getDate(){
        return this.date;
    }

    /**
     * Re-captions a photo
     *
     * @param newCaption
     */
    public void setCaption(String newCaption){
        //ALERTS
        if(newCaption.length() == 0){
            PhotosController.alert("Text Error", "Please enter the new caption");
            return;
        }

        this.caption = newCaption;
    }

    /**
     * Adds a tag to the photo
     *
     * @param newTag
     */
    public void addTagToPhoto(Tag newTag){
        listOfTags.add(newTag);
    }

    /**
     * Deletes tag from a photo
     *
     * @param tag
     */
    public void deleteTagFromPhoto(String tag){

        //ALERTS
        if(tag.length() == 0){
            PhotosController.alert("Text Error", "Please enter a tag");
            return;
        }

        /**
         * Retrieve the tag that has to be deleted from photo
         */
        Tag t = getTag(tag);
        if(t == null){
            PhotosController.alert("Tag Error", "Tag does not exist");
            return;
        }
        listOfTags.remove(t);
    }

    /**
     *
     * @return Photo's summary
     */
    public String toString(){
        return "Caption:\n" + this.getCaption() + "\n\nDate of Photo:\n" + this.getDate() + "\n\nTags:\n" +
                this.printListOfTags();
    }

    /**
     * Brings user back to list of photos in album
     *
     * @param event
     * @throws IOException
     */
    public void backToPhotos(ActionEvent event) throws IOException {
        FXMLLoader loader = new FXMLLoader();
        loader.setLocation(getClass().getResource("/photos/src/view/Photos9Gen.fxml"));
        Parent root = loader.load();
        Scene scene = new Scene(root);
        Stage stage = (Stage) ((Node)event.getSource()).getScene().getWindow();
        stage.setScene(scene);
        stage.show();

        Album album = loader.getController();
        album.start(event);
    }

    /**
     * Sets up the photo's page
     *
     * @param event
     * @param currentPhoto
     */
    public void start(ActionEvent event, Photo currentPhoto){
        photoDetailsField.setText(currentPhoto.toString());
        photoDisplay.setImage(new Image("file:" + currentPhoto.getPath().toString(), 400, 400, true, true));
    }

}




