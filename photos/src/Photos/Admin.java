package photos.src.Photos;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.ListView;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import photos.data.Database;

import java.io.IOException;

import static photos.src.Photos.PhotosController.confirm;

/**
 * Admin Class
 * Contains fields/methods for the admin account
 *
 * @author Ashleigh Chung
 * @author Jason Cheng
 */
public class Admin {
//    static final long serialVersionUID  = 1L;
    /**
     * To retrieve the text to create users
     */
    @FXML
    TextField createUserField;
    /**
     * To retrieve the text to delete users
     */
    @FXML
    TextField deleteUserField;
    /**
     * To display the listview
     */
    @FXML
    ListView<String> listView;

    /**
     * Creates a new User obj and adds it to the list
     *
     * @param event
     */
    @FXML
    public void createUser(ActionEvent event) {
        String username = createUserField.getText().strip();
        //ALERTS
        if(username.equals("admin")){
            PhotosController.alert("Name Error", "Cannot create the Admin Account");
            createUserField.clear();
            return;
        }
        if (username.length() == 0) {
            PhotosController.alert("Name Error", "Name cannot be left empty");
            createUserField.clear();
            return;
        }
        for (int i = 0; i < Photos.listOfUsers.size(); i++) {
            if (Photos.listOfUsers.get(i).username.equals(username)) {
                PhotosController.alert("Name Error", "Cannot have Users with same username");
                createUserField.clear();
                return;
            }
        }
        /**
         * New User obj
         */
        User newUser = new User(username);
        Photos.listOfUsers.add(newUser);
        update();
        createUserField.clear();
    }

    /**
     * Deletes a user by removing it from the list
     *
     * @param event
     */
    @FXML
    public void deleteUser(ActionEvent event) {
        /**
         * deleteUserField
         */
        String username = deleteUserField.getText().strip();
        if(username.equals("stock")){
            PhotosController.alert("Name Error", "Cannot delete the Stock Account");
            deleteUserField.clear();
            return;
        }
        if(username.equals("admin")){
            PhotosController.alert("Name Error", "Cannot delete the Admin Account");
            deleteUserField.clear();
            return;
        }
        for (int i = 0; i < Photos.listOfUsers.size(); i++) {
            if (Photos.listOfUsers.get(i).username.equals(username)) {
                Photos.listOfUsers.remove(i);
                update();
                deleteUserField.clear();
                return;
            }
        }
        //ALERTS
        PhotosController.alert("Name Error", "There is no User with this username");
        deleteUserField.clear();
        return;
    }

    /**
     * No args constructor for FXML and JavaFX
     */
    public Admin(){
        //Important Don't Delete
    }

    /**
     * Wrapper of the update method so the method can be called statically
     *
     */
    public void start(){
        update();
    }

    /**
     * Updates the Listview
     */
    public void update(){
        ObservableList<String> list = FXCollections.observableArrayList();
        for (User user : Photos.listOfUsers) {
            list.add(user.getUsername());
        }
        listView.setItems(list);
    }
    /**
     * Handles logout from the admin page
     * @param event
     * @throws IOException
     */
    public void logout(ActionEvent event) throws IOException {
        if(confirm("Logout Confirmation", "Are you sure that you want to logout?")) {
            Database.write(Photos.listOfUsers);
            Parent root = FXMLLoader.load(Admin.class.getResource("/photos/src/view/Photos2Login.fxml"));
            Scene scene = new Scene(root);
            Stage stage = (Stage) ((Node)event.getSource()).getScene().getWindow();
            stage.setScene(scene);
            stage.show();
        }
    }

    /**
     * Handles quitting from the admin page
     * @param event
     * @throws IOException
     */
    public void quit(ActionEvent event) throws IOException {
        if(confirm("Closing Application", "Are you sure that you want to quit the application?")) {
            Database.write(Photos.listOfUsers);
            Stage stage = (Stage) ((Node)event.getSource()).getScene().getWindow();
            stage.close();
        }
    }


}





