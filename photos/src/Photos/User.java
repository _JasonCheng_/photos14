package photos.src.Photos;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.ListView;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import photos.data.Database;

import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;

import static photos.src.Photos.PhotosController.confirm;

/**
 * User Class
 * Contains fields/methods for the user account
 *
 * @author Ashleigh Chung
 */
public class User implements Serializable {

    /**
     * Used for serialization
     */
    static final long serialVersionUID  = 1L;

    /**
     * User's username
     */
    public String username;

    /**
     * User's list of albums
     */
    public ArrayList<Album> listOfAlbums;

    /**
     * List of pre-defined tags the User can choose from when creating a new tag
     */
    public ArrayList<String> sampleTagNames;

    /**
     * List of tags the User can choose from when creating a new tag
     */
    public ArrayList<Tag> listOfTags;

    /**
     * Index of user
     */
    int currIndex;

    //CREATEALBUM
    /**
     * create album field
     */
    @FXML
    TextField createAlbumField;

    //DELETEALBUM
    /**
     * delete album field
     */
    @FXML
    TextField deleteAlbumField;

    //RENAMEALBUM
    /**
     * renameAlbumOld Field
     */
    @FXML
    TextField renameAlbumOldField;

    /**
     * rename album new field
     */
    @FXML
    TextField renameAlbumNewField;

    /**
     * listView
     */
    @FXML
    ListView<String> listView;

    /**
     * Text area showing album details
     */
    @FXML
    TextArea albumDetails;

    /**
     * Search button controller
     */
    @FXML
    private Search searchButtonController;

    /**
     * User constructor
     *
     * @param username
     */
    public User(String username){
        this.username = username;
        this.listOfAlbums = new ArrayList<Album>();
        this.sampleTagNames = new ArrayList<String>();
        this.listOfTags = new ArrayList<Tag>();
        sampleTagNames.add("Location");
        sampleTagNames.add("Food");
        sampleTagNames.add("DayOfTheWeek");
    }

    /**
     * @return username
     */
    public String getUsername(){
        return this.username;
    }

    /**
     * @return listOfAlbums
     */
    public ArrayList<Album> getListOfAlbums(){
        return this.listOfAlbums;
    }

    /**
     * Returns the album based on the album name
     *
     * @param albumName
     * @return Album
     */
    public Album getAlbum(String albumName){//this is not the oop getAlbum
        for(int i = 0; i < Photos.listOfAlbums.size(); i++){
            if(Photos.listOfAlbums.get(i).getAlbumName().equals(albumName))
                return Photos.listOfAlbums.get(i);
        }
        return null;
    }

    /**
     * @return listOfTags
     */
    public ArrayList<Tag> getListOfTags(){
        return listOfTags;
    }

    /**
     * Creates an album for the User
     *
     * @param event
     */
    @FXML
    public void createAlbum(ActionEvent event){
        /**
         * Name of new album
         */
        String albumName = createAlbumField.getText().strip();

        //ALERTS
        if(albumName.length() == 0){
            PhotosController.alert("Name Error", "Name cannot be left empty");
            createAlbumField.clear();
            return;
        }
        for(int i = 0; i < Photos.listOfAlbums.size(); i++){
            if(Photos.listOfAlbums.get(i).albumName.equals(albumName)){
                PhotosController.alert("Name Error", "Cannot have album with same name");
                createAlbumField.clear();
                return;
            }
        }

        /**
         * New Album obj
         */
        Album newAlbum = new Album(albumName);
        Photos.listOfAlbums.add(newAlbum);
        update();
        createAlbumField.clear();
    }

    /**
     * Deletes album from User's list
     *
     * @param event
     */
    @FXML
    public void deleteAlbum(ActionEvent event){
        /**
         * Name of deleted album
         */
        String albumName = deleteAlbumField.getText().strip();
        if(Photos.currentUser.username.equals("stock") && albumName.equals("Stock Pictures")){
            PhotosController.alert("Album Error", "You cannot delete the Stock album from the Stock Account");
            return;
        }
        for(int i = 0; i < Photos.listOfAlbums.size(); i++){
            if(Photos.listOfAlbums.get(i).getAlbumName().equals(albumName)){
                Photos.listOfAlbums.remove(i);
                update();
                deleteAlbumField.clear();
                return;
            }
        }
        //ALERTS
        PhotosController.alert("Name Error", "There is no Album with this name");
        deleteAlbumField.clear();
        return;
    }

    /**
     * Renames album
     *
     * @param event
     */
    @FXML
    public void renameAlbum(ActionEvent event){
        /**
         * old name of album
         */
        String oldName = renameAlbumOldField.getText();
        /**
         * new Name of album
         */
        String newName = renameAlbumNewField.getText().strip();

        if(Photos.currentUser.username.equals("stock") && oldName.equals("Stock Pictures")){
            PhotosController.alert("Album Error", "You cannot rename the Stock Pictures Album");
            return;
        }
        //ALERTS
        if(newName.length() == 0 || oldName.length() == 0){
            PhotosController.alert("Name Error", "Either name cannot be left empty");
            renameAlbumNewField.clear();
            return;
        }
        for(int i = 0; i < Photos.listOfAlbums.size(); i++){
            if(Photos.listOfAlbums.get(i).getAlbumName().equals(newName)){
                PhotosController.alert("Name Error", "An album already shares the same name.");
                renameAlbumNewField.clear();
                return;
            }
        }
        /**
         * Album we are renaming
         */
        Album a = getAlbum(oldName);

        if(a == null){
            PhotosController.alert("Name Error", "Album with old name does not exist");
            renameAlbumNewField.clear();
            return;
        }

        a.setAlbumName(newName);
        update();
        listView.getSelectionModel().clearSelection(currIndex);
        renameAlbumOldField.clear();
        renameAlbumNewField.clear();

    }

    /**
     * User constructor
     */
    public User(){
        //Don't Delete Left Empty for FXML
    }

    /**
     * Start the list of albums page
     *
     * @param event
     */
    public void start(ActionEvent event){
        update();
        listView.getSelectionModel().selectedIndexProperty().addListener((obs, oldVal, newVal) -> displayAlbum());
    }

    /**
     * updates the list of album's page
     */
    public void update(){
        /**
         * FXML element
         */
        ObservableList<String> list = FXCollections.observableArrayList();
        for (Album album : Photos.listOfAlbums) {
            list.add(album.getAlbumName());
        }
        listView.setItems(list);
    }

    /**
     * Displays album
     */
    public void displayAlbum(){
        currIndex = listView.getSelectionModel().getSelectedIndex();
        if(currIndex == -1){
            return;
        }
        /**
         * Album we are selecting
         */
        Album currAlbum = Photos.listOfAlbums.get(currIndex);
        Photos.listOfPhotos = currAlbum.getListOfPhotos();
        Photos.albumName = currAlbum.getAlbumName();
        showAlbumDetails(currIndex);
    }

    /**
     * Shows the album's details
     * @param curr_index
     */
    public void showAlbumDetails(int curr_index){//when user selects song in the listview
        /**
         * Album we are selecting
         */
        Album album = Photos.listOfAlbums.get(curr_index);
        String context = album.printAlbumSummary();
        albumDetails.setText(context);
        Photos.currentAlbum = Photos.listOfAlbums.get(curr_index);
    }

    /**
     * Login Method
     *
     * @param event
     * @param username
     * @return
     * @throws IOException
     * @throws ClassNotFoundException
     */
    public static boolean login(ActionEvent event, String username) throws IOException, ClassNotFoundException {
        for (User user : Photos.listOfUsers){
            if(user.username.equals(username)){
                Photos.currentUser = user;
                Photos.userName = username;
                Photos.listOfAlbums = user.getListOfAlbums();
                return true;
            }
        }
        return false;//calls the scene
    }

    /**
     * Logout Method
     *
     * @param event
     * @throws IOException
     */
    public void logout(ActionEvent event) throws IOException {
        if(confirm("Logout Confirmation", "Are you sure that you want to logout?")) {
            save();
            Database.write(Photos.listOfUsers);
            Parent root = FXMLLoader.load(Admin.class.getResource("/photos/src/view/Photos2Login.fxml"));
            Scene scene = new Scene(root);
            Stage stage = (Stage) ((Node)event.getSource()).getScene().getWindow();
            stage.setScene(scene);
            stage.show();
        }
    }

    /**
     * Quit method
     *
     * @param event
     * @throws IOException
     */
    public void quit(ActionEvent event) throws IOException {
        if(confirm("Closing Application", "Are you sure that you want to quit the application?")) {
            save();
            Database.write(Photos.listOfUsers);
            Stage stage = (Stage) ((Node)event.getSource()).getScene().getWindow();
            stage.close();
        }
    }

    /**
     * Save method
     */
    public void save(){
        for(int i=0; i < Photos.listOfUsers.size(); i++){
            if(Photos.listOfUsers.get(i).username.equals(Photos.userName)){
                Photos.listOfUsers.get(i).listOfAlbums = Photos.listOfAlbums;
            }
        }
    }

    /**
     * When user clicks on search
     *
     * @param event
     * @throws IOException
     */
    @FXML
    public void search(ActionEvent event) throws IOException {
        FXMLLoader loader = new FXMLLoader();
        loader.setLocation(getClass().getResource("/photos/src/view/Photos6Gen.fxml"));
        Parent root = loader.load();
        Scene scene = new Scene(root);
        Stage stage = (Stage) ((Node)event.getSource()).getScene().getWindow();
        stage.setScene(scene);
        stage.show();
    }

    /**
     * When user clicks on open
     *
     * @param event
     * @throws IOException
     */
    @FXML
    public void openAlbum(ActionEvent event) throws IOException {
        if(listView.getSelectionModel().getSelectedIndex() == -1){
            PhotosController.alert("Album Error", "Need to select an album first");
            return;
        }
        FXMLLoader loader = new FXMLLoader();
        loader.setLocation(getClass().getResource("/photos/src/view/Photos9Gen.fxml"));
        Parent root = loader.load();
        Scene scene = new Scene(root);
        Stage stage = (Stage) ((Node)event.getSource()).getScene().getWindow();
        stage.setScene(scene);
        stage.show();

        Album album = loader.getController();
        album.start(event);
    }
}
