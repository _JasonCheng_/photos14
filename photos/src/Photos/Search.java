package photos.src.Photos;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.Priority;
import javafx.scene.layout.Region;
import javafx.scene.layout.TilePane;
import javafx.scene.layout.VBox;
import javafx.scene.text.Font;
import javafx.scene.text.Text;
import javafx.stage.Stage;

import java.io.IOException;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Date;

/**
 * Search Class
 * Contains fields/methods for a Search
 *
 * @author Ashleigh Chung
 */
public class Search {

    /**
     * A list of searched photo
     */
    public static ArrayList<Photo> listOfSearchedPhotos;

    //SEARCH BY TAG
    /**
     * tag value 1 field
     */
    @FXML
    TextField tagValue1Field;

    /**
     * tag value 2 field
     */
    @FXML
    TextField tagValue2Field;

    /**
     * or/add field
     */
    @FXML
    TextField orAddField;

    //SEARCH BY DATE
    /**
     * date 1 field
     */
    @FXML
    TextField date1Field;

    /**
     * date 2 field
     */
    @FXML
    TextField date2Field;

    //CREATE SEARCH ALBUM
    /**
     * album name field
     */
    @FXML
    TextField createSearchAlbumField;

    //TILE PANE
    /**
     * search tile pane
     */
    @FXML
    TilePane searchTilePane;

    /**
     * search scroll pane
     */
    @FXML
    ScrollPane searchScrollPane;

    /**
     * Searching for photos by Tag
     *
     * @param event
     * @throws IOException
     */
    @FXML
    public void searchByTag(ActionEvent event) throws IOException {
        listOfSearchedPhotos = new ArrayList<>();

        //Search by Tag Fields
        /**
         * Whether its 'or' or 'and'
         */
        boolean isOr = true;
        /**
         * Whether or not user is using two fields or one
         */
        boolean boolFilled = false;

        /**
         * full date 1
         */
        String fullTag1 = tagValue1Field.getText();
        /**
         * full date 2
         */
        String fullTag2 = tagValue2Field.getText();
        /**
         * boolean answer
         */
        String orAdd = orAddField.getText();

        //ALERTS
        if (fullTag1.length() == 0) {
            PhotosController.alert("Text Error", "Please fill out all fields");
            return;
        }
        if (orAdd.length() == 0 && fullTag2.length() != 0) {
            PhotosController.alert("Text Error", "Please fill out the second field");
            return;
        }
        if (fullTag2.length() == 0 && orAdd.length() != 0) {
            PhotosController.alert("Text Error", "Please fill out the third field");
            return;
        }
        if(fullTag1.equals(fullTag2)){
            PhotosController.alert("Text Error", "Please make Tag #2 different from Tag #1");
            return;
        }
        if(!orAdd.equals("") && (!orAdd.equals("or") && !orAdd.equals("and"))){
            PhotosController.alert("Text Error", "Please type in 'or' or 'and' in the second field");
            return;
        }
        if(!fullTag1.contains("=") || fullTag1.length() < 3 || fullTag1.indexOf("=") == 0 || fullTag1.indexOf("=") == fullTag1.length() - 1){
            PhotosController.alert("Text Error", "Please follow tag format for Tag #1");
            return;
        }
        if(fullTag2.length() != 0 && (!fullTag2.contains("=") || fullTag2.length() < 3 || fullTag2.indexOf("=") == 0 || fullTag2.indexOf("=") == fullTag2.length() - 1)){
            PhotosController.alert("Text Error", "Please follow tag format for Tag #2");
            return;
        }

        //Parse boolean
        if(!orAdd.equals("")){
            if(orAdd.equals("or")) isOr = true;
            else isOr = false;
            boolFilled = true;
        }

        //Loop through all Albums
        for(int i = 0; i < Photos.listOfAlbums.size(); i++){
            /**
             * Each album we traverse through
             */
            Album a = Photos.listOfAlbums.get(i);

            //Loop through all Photos in album
            for(int j = 0; j < a.listOfPhotos.size(); j++){
                /**
                 * Whether tag1 is found
                 */
                boolean t1Found = false;
                /**
                 * Whether tag2 is found
                 */
                boolean t2Found = false;
                /**
                 * Each photo we traverse through
                 */
                Photo ptemp = Photos.listOfAlbums.get(i).listOfPhotos.get(j);

                //Loop through all Tags in photo
                for(int t = 0; t < ptemp.listOfTags.size(); t++){
                    /**
                     * Each tag we check
                     */
                    Tag temp = Photos.listOfAlbums.get(i).listOfPhotos.get(j).listOfTags.get(t);

                    //Find Tag 1
                    if(!t1Found && temp.fullTag.equals(fullTag1)){
                        t1Found = true;
                        if(!boolFilled || isOr){
                            listOfSearchedPhotos.add(ptemp);
                            break;
                        }
                    }

                    //Find Tag 2
                    if(temp.fullTag.equals(fullTag2)){
                        t2Found = true;
                        if(!(isOr == false && t1Found == false)){
                            listOfSearchedPhotos.add(ptemp);
                            break;
                        }
                    }
                }
                if(isOr == false && t2Found == true && t1Found == true)
                    listOfSearchedPhotos.add(ptemp);
            }
        }

        if(listOfSearchedPhotos.size() == 0){
            PhotosController.alert("Input Error", "There are no photos with these tag/tags. Please enter new tags");
            return;
        }

        FXMLLoader loader = new FXMLLoader();
        loader.setLocation(getClass().getResource("/photos/src/view/Photos6Gen.fxml"));
        Parent root = null;
        root = loader.load();
        Scene scene = new Scene(root);
        Stage stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
        stage.setScene(scene);
        stage.show();

        Search search = loader.getController();
        search.start(event);
    }

    /**
     * Searching tags by dates
     *
     * @param event
     * @throws IOException
     */
    @FXML
    public void searchByDate (ActionEvent event) throws IOException {
        listOfSearchedPhotos = new ArrayList<>();

        //yyyy-mm-dd
        /**
         * Date 1 answer
         */
        String date1 = date1Field.getText().trim();
        /**
         * Date 2 answer
         */
        String date2 = date2Field.getText().trim();

        //ALERTS
        if (date1.length() == 0 || date2.length() == 0) {
            PhotosController.alert("Text Error", "Please fill out all fields");
            return;
        }
        if(date1.length() != 10 || date2.length() != 10) {
            PhotosController.alert("Text Error", "Please fill out all fields in the correct format");
            return;
        }
        if( !Character.isDigit(date1.charAt(0)) || !Character.isDigit(date1.charAt(1)) || !Character.isDigit(date1.charAt(2)) || !Character.isDigit(date1.charAt(3)) || !date1.substring(4,5).equals("-") || !Character.isDigit(date1.charAt(5)) || !Character.isDigit(date1.charAt(6)) || !date1.substring(7,8).equals("-") || !Character.isDigit(date1.charAt(8)) || !Character.isDigit(date1.charAt(9))){
            PhotosController.alert("Text Error", "Please follow tag format for Date #1");
            return;
        }
        if( !Character.isDigit(date2.charAt(0)) || !Character.isDigit(date2.charAt(1)) || !Character.isDigit(date2.charAt(2)) || !Character.isDigit(date2.charAt(3)) || !date2.substring(4,5).equals("-") || !Character.isDigit(date2.charAt(5)) || !Character.isDigit(date2.charAt(6)) || !date2.substring(7,8).equals("-") || !Character.isDigit(date2.charAt(8)) || !Character.isDigit(date2.charAt(9))){
            PhotosController.alert("Text Error", "Please follow tag format for Date #2");
            return;
        }
        if(Integer.parseInt(date1.substring(5, 7)) > 12 || Integer.parseInt(date1.substring(8, 10)) > 31 || (date1.charAt(8) == '0' && date1.charAt(9) == '0')){
            PhotosController.alert("Text Error", "Please input appropriate month and day numbers");
            return;
        }
        if(Integer.parseInt(date2.substring(5, 7)) > 12 || Integer.parseInt(date2.substring(8, 10)) > 31 || (date2.charAt(8) == '0' && date2.charAt(9) == '0')){
            PhotosController.alert("Text Error", "Please input appropriate month and day numbers");
            return;
        }

        /**
         * Convert date1 to LocalDate
         */
        LocalDate d1 = LocalDate.parse(date1);
        /**
         * Convert date2 to LocalDate
         */
        LocalDate d2 = LocalDate.parse(date2);

        if(d1.isAfter(d2)){
            PhotosController.alert("Text Error", "Date #1 cannot be later than Date #2.");
            return;
        }

        //Loop through all Albums
        for(int i = 0; i < Photos.listOfAlbums.size(); i++){
            /**
             * Album we are traversing through
             */
            Album a = Photos.listOfAlbums.get(i);
            //Loop through all Photos in album
            for(int j = 0; j < a.listOfPhotos.size(); j++){
                if(a.listOfPhotos.get(j) != null){
                    /**
                     * Photo we are comparing
                     */
                    Photo ptemp = a.listOfPhotos.get(j);
                    /**
                     * Date of photo
                     */
                    Date d = ptemp.getDate();
                    /**
                     * Date converted into LocalDate
                     */
                    LocalDate ld = LocalDate.of(d.getYear()+1900, d.getMonth()+1, d.getDay()+12);

                    if((d1.isBefore(ld) || d1.isEqual(ld)) && (d2.isAfter(ld) || (d2.isEqual(ld)))){
                        listOfSearchedPhotos.add(ptemp);
                    }
                }
            }
        }
        if(listOfSearchedPhotos.size() == 0){
            PhotosController.alert("Input Error", "There are no photos within these two dates. Please enter new dates");
            return;
        }

        FXMLLoader loader = new FXMLLoader();
        loader.setLocation(getClass().getResource("/photos/src/view/Photos6Gen.fxml"));
        Parent root = null;
        root = loader.load();
        Scene scene = new Scene(root);
        Stage stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
        stage.setScene(scene);
        stage.show();

        Search search = loader.getController();
        search.start(event);
    }

    /**
     * Set up Search page
     *
     * @param event
     */
    public void start(ActionEvent event){
        update(event);
    }

    /**
     * Set up tilepane
     *
     * @param event
     */
    public void update(ActionEvent event){
        searchTilePane.getChildren().clear();
//        searchTilePane = new TilePane();
//        searchTilePane.setHgap(20);
//        searchTilePane.setVgap(20);
//        searchTilePane.setPadding(new Insets(20));
//        searchTilePane.setPrefColumns(2);
        for (int i = 0; i < listOfSearchedPhotos.size(); i++) {
            /**
             * Photo we want to add to tilepane
             */
            Photo photo = listOfSearchedPhotos.get(i);
            /**
             * Convert Photo to image
             */
            Image img = new Image("file:" + photo.getPath(), 200, 200, true, true);
            /**
             * Add image to imageview
             */
            ImageView iview = new ImageView(img);
            iview.setFitWidth(225);
            iview.setFitHeight(200);
            iview.setPreserveRatio(true);
            /**
             * Caption of photo
             */
            Text txt = new Text(photo.getCaption());
            txt.setFont(new Font("Times New Roman", 10));
            /**
             * Space between photo and caption
             */
            Region spacer = new Region();
            /**
             * Where caption and image sit together
             */
            VBox box = new VBox(10, iview, spacer, txt);
            box.setVgrow(spacer, Priority.ALWAYS);
            box.setAlignment(Pos.CENTER);

            searchTilePane.getChildren().add(box);
        }
        searchScrollPane.setContent(searchTilePane);
    }

    /**
     * Creates an album full of pictures for the User
     *
     * @param event
     */
    @FXML
    public void createSearchAlbum(ActionEvent event) throws IOException {
        /**
         * album name of new album
         */
        String albumName = createSearchAlbumField.getText();

        //ALERTS
        if(albumName.length() == 0){
            PhotosController.alert("Name Error", "Name cannot be left empty");
            return;
        }
        for(int i = 0; i <Photos.listOfAlbums.size(); i++){
            if(Photos.listOfAlbums.get(i).albumName.equals(albumName)){
                PhotosController.alert("Name Error", "Cannot have album with same name");
                return;
            }
        }
        if(listOfSearchedPhotos == null || listOfSearchedPhotos.size() == 0){
            PhotosController.alert("Selection Error", "Please select photos to create an album of");
            return;
        }

        /**
         * New Album obj
         */
        Album newAlbum = new Album(albumName, listOfSearchedPhotos);
        Photos.listOfAlbums.add(newAlbum);

        FXMLLoader loader = new FXMLLoader();
        loader.setLocation(getClass().getResource("/photos/src/view/Photos5Gen.fxml"));
        Parent root = loader.load();
        Scene scene = new Scene(root);
        Stage stage = (Stage) ((Node)event.getSource()).getScene().getWindow();
        stage.setScene(scene);
        stage.show();

        User user = loader.getController();
        user.start(event);
    }

    /**
     * Brings User back to list of Albums
     *
     * @param event
     * @throws IOException
     */
    @FXML
    public void backToAlbums(ActionEvent event) throws IOException {
        FXMLLoader loader = new FXMLLoader();
        loader.setLocation(getClass().getResource("/photos/src/view/Photos5Gen.fxml"));
        Parent root = loader.load();
        Scene scene = new Scene(root);
        Stage stage = (Stage) ((Node)event.getSource()).getScene().getWindow();
        stage.setScene(scene);
        stage.show();

        User user = loader.getController();
        user.start(event);
    }

    /**
     * Brings user to search tag popup
     * @param event
     * @throws IOException
     */
    @FXML
    public void toSearchTag(ActionEvent event) throws IOException {
        FXMLLoader loader = new FXMLLoader();
        loader.setLocation(getClass().getResource("/photos/src/view/PhotosSearchTag.fxml"));
        Parent root = loader.load();
        Scene scene = new Scene(root);
        Stage stage = (Stage) ((Node)event.getSource()).getScene().getWindow();
        stage.setScene(scene);
        stage.show();
    }

    /**
     * Brings user to search date popup
     * @param event
     * @throws IOException
     */
    @FXML
    public void toSearchDate(ActionEvent event) throws IOException {
        FXMLLoader loader = new FXMLLoader();
        loader.setLocation(getClass().getResource("/photos/src/view/PhotosSearchDate.fxml"));
        Parent root = loader.load();
        Scene scene = new Scene(root);
        Stage stage = (Stage) ((Node)event.getSource()).getScene().getWindow();
        stage.setScene(scene);
        stage.show();
    }

    /**
     * Allows user to exit popup
     *
     * @param event
     * @throws IOException
     */
    @FXML
    public void backToSearch(ActionEvent event) throws IOException {
        FXMLLoader loader = new FXMLLoader();
        loader.setLocation(getClass().getResource("/photos/src/view/Photos6Gen.fxml"));
        Parent root = loader.load();
        Scene scene = new Scene(root);
        Stage stage = (Stage) ((Node)event.getSource()).getScene().getWindow();
        stage.setScene(scene);
        stage.show();
    }

}

