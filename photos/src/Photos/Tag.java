package photos.src.Photos;

import java.io.Serializable;

/**
 * Tag Class
 * Contains fields/methods for a tag
 *
 * @author Ashleigh Chung
 */
public class Tag implements Serializable {
    /**
     * Used for serialization
     */
    static final long serialVersionUID  = 1L;

    /**
     * The whole "tagName=tagValue"
     */
    String fullTag;

    /**
     * tagName from "tagName=tagValue"
     */
    String tagName;

    /**
     * tagValue from "tagName=tagValue"
     */
    String tagValue;

    /**
     * Whether there can only be one of these tagNames for this photo
     */
    boolean onlyOneValue;

    /**
     * Tag constructor
     * Also parses the fullTag
     *
     * @param fullTag
     * @param onlyOneValue
     */
    public Tag(String fullTag, boolean onlyOneValue){
        this.fullTag = fullTag;
        this.tagName = fullTag.substring(0, fullTag.indexOf("="));
        this.tagValue = fullTag.substring(fullTag.indexOf("=") + 1);
        this.onlyOneValue = onlyOneValue;
    }

    /**
     * @return tagName
     */
    public String getTagName(){
        return tagValue;
    }

    /**
     * @return tagValue
     */
    public String getTagValue(){
        return tagName;
    }

    /**
     * Print out tag's fullTag
     *
     * @return
     */
    public String toString() {
        return fullTag;
    }
}
